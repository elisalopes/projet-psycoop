<?php
    session_start();
    include 'request.php';
    if (empty($_COOKIE["connected"]) && empty($_SESSION["connected"])) {
        header("location: index.php");
    }
    if (isset($_POST['disconnect'])) {  
        setcookie('connected', FALSE);  
        $_COOKIE["connected"]=FALSE;
        $_SESSION["connected"]=FALSE;
        header("location: index.php");
    }
    if (isset($_POST['ranking'])) {  
        header("location: ranking.php");
    }
    if (isset($_POST['acceuil'])) {  
        header("location: acceuil.php");
    }
?>

<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <link rel="stylesheet" href="../CSS/index.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div id='main'>   
        <header>
            <div>
                <form action="<?=$_SERVER["PHP_SELF"]; ?>"method="POST">
                    <button class='bouton_header' type="submit" name="disconnect">Disconnect</button>
                    <button class='bouton_header' type="submit" name="ranking">Classement</button>
                    <button class='bouton_header' id="acceuil_button" type="submit" name="acceuil" style="display: none">Acceuil</button>
                </form>
                <h1 id="title">Acceuil</h1>
            </div>
        
  
        </header>
        <div class="main_content">
            <div id="play">
                <button id='play_button' name="play"><h1>Play</h1></button>
                <span>
                    <input type="checkbox" name="hard_mode" id="hard_mode"> Hard Mode
                </span>
            </div>    
            <div class="history_table">
                <table>
                    <tr>
                        <th>Last Game</th>
                        <th>Score</th>
                        <th>Détails</th>
                    </tr>
                    <?php
                        $historical =  get_historical($_COOKIE['id_user']);
                        for ($i = 0; $i < count($historical); $i++) {
                            $percent_coop = get_percentage_coop($historical[$i]['id_game']);
                            if ($historical[$i]['level'] == 0) {
                                $level_name = "Easy";
                            } else {
                                $level_name = "Hard";
                            }?>
                            <tr>
                                <td><?=($i + 1);?></td>
                                <td><?=$historical[$i]['score'];?></td>
                                <td><button class='button_detail' onmouseover="show_detail(<?=$i;?>)" onmouseout="hide_detail(<?=$i;?>)">Détails</button></td>
                                <div id ="detail_value<?=$i;?>" style="display: none" class="detail_game">
                                <?="Coop: ".round($percent_coop) . "% <br>Trahison: " . round(100 - $percent_coop) . "%<br>Difficulté: ".$level_name;?>
                                </div>
                            </tr>
                            <?php
                        }
                    ?>
                </table>
            </div>
        </div>
        <div class="game_content" style="display: none">
            <div class="dialog_box">
                <span id="strategy"></span>
                <span id="dialog_text"><br>6 pièces tombent du ciel entre vous et l'adversaire<br>
                    Vous avez le choix, partager le butin ou tout rafler<br>
                    Faites votre choix<br></span>
                <span id="round"></span>
                <span id="user_score">Votre Score</span>
                <span id="cpu_score">Score du CPU</span>
            </div>
            <div class="User_Choice">
                <button id="betrayal_button" onclick="selected_option(BETRAYAL)"><h1>Trahir</h1></button>
                <button id="coop_button" onclick="selected_option(COOPERATION)"><h1>Coopérer</h1></button>
            </div>
            <button id="replay"><h1>Rejouer</h1></button>
            <div class="Strategy_Box">
                <button id="lunatique" onmouseover="show_strategy(this,'Coopère ou trahit au hasard!')" onmouseout="hide_strategy()">Lunatique</button>
                <button id="periodique_gentil" onmouseover="show_strategy(this, 'Coopère, coopère et trahit périodiquement!')" onmouseout="hide_strategy()">Périodique-gentil</button>
                <button id="periodique_mechant" onmouseover="show_strategy(this,'Trahit, trahit et coopère périodiquement!')" onmouseout="hide_strategy()">Périodique-méchant</button>
                <button id="binaire" onmouseover="show_strategy(this, 'Coopère puis trahit périodiquement!')" onmouseout="hide_strategy()">Binaire</button>
                <button id="majorite" onmouseover="show_strategy(this, 'Coopère au premier tour, coopère également si l\'adversaire a trahit et coopérer autant de fois sinon Majorité joue majoritairement ce que l\'adversaire à joué!')" onmouseout="hide_strategy()">Majorité</button>
                <button id="sondeur" onmouseover="show_strategy(this, 'Trahit au premier tour, coopère au second tour, coopère au troisième tour puis si l\'adversaire a coopérer au deuxième et troisième tour, il trahit toujours sinon il joue donnant-donnant!')" onmouseout="hide_strategy()">Sondeur</button>
                <button id="cccct" onmouseover="show_strategy(this, 'Coopère, coopère, coopère, coopère et trahit périodiquement!')" onmouseout="hide_strategy()">CCCCT</button>
                <button id="donnant_donnant" onmouseover="show_strategy(this, 'Coopère au premier tour puis à partir du deuxième tour il joue ce que l`adversaire a joué au tour précédent!')" onmouseout="hide_strategy()">Donnant-donnant</button> 
            </div>
            <div id="final_result">
            </div>
            <div id="cpu_strategy">
            </div>
            <div class="graph" style="position: relative; height:30vh; width:40vw">
                <canvas id="graph_game" width="" height=""></canvas>
            </div>
        </div>
    </div>
    
<script>
    let strategy = [lunatique, periodique_gentil, periodique_mechant, donnant_donnant, sondeur, majorite, binaire, cccct];
    $(document).ready(function(){
        $("#play_button").click(function(){
            $("#title").text("Jeu");
            $(".main_content").hide();
            $(".game_content").show();
            $("#acceuil_button").show();
            $('#replay').hide();
            hard_mode=document.getElementById("hard_mode").checked;
            if (hard_mode==false) {
                document.getElementById("strategy").innerHTML="La stratégie du CPU est: <b>" + cpu_strategy.name.replace("_","-") + "</b>";
            }
        });
        $("#replay").click(function(){
            location.reload();
        });
    });

    const COOPERATION = 1;
    const BETRAYAL = 0;
    const ROUND_NUMBER = 15;
    let current_round = 0;
    let user_data = [];
    let cpu_data = [];
    let cpu_strategy = get_cpu_strategy();
    CPU_choice = [BETRAYAL, COOPERATION];
    let user_score=0;
    let cpu_score=0;
    let user_score_table = [0];
    let cpu_score_table = [0];
    let hard_mode = false;
    let user_strategy = 0;
    
    
    // =======================================================================================
    // FONCTIONS PRINCIPALES 
    show_gameplay_round();
    function selected_option (choice) {
        let cpu_choice = get_cpu_choice();
        cpu_data.push(cpu_choice);
        user_data.push(choice);
        display_score();
        show_graph();
        current_round++;
        display_round();
        display_user_strategy();
        if (current_round == ROUND_NUMBER) {
            end_game();
        }
        let last_cpu_choice = cpu_data[cpu_data.length-1];
        let last_user_choice = user_data[user_data.length-1];
        if (choice==BETRAYAL && last_cpu_choice==BETRAYAL){
            document.getElementById("dialog_text").innerHTML='<span id="user_choice">Vous avez trahi</span><span id="cpu_choice">Le CPU vous a trahi</span><span id="coin">Pièce gagné: 1</span>';
        } else if (choice==BETRAYAL && last_cpu_choice==COOPERATION){
            document.getElementById("dialog_text").innerHTML='<span id="user_choice">Vous avez trahi</span><span id="cpu_choice">Le CPU a coopéré</span><span id="coin">Pièces gagnés: 5</span>';
        } else if (choice==COOPERATION && last_cpu_choice==COOPERATION){
            document.getElementById("dialog_text").innerHTML='<span id="user_choice">Vous avez coopéré</span><span id="cpu_choice">Le CPU a coopéré</span><span id="coin">Pièces gagnés: 3</span>';
        } else if (choice==COOPERATION && last_cpu_choice==BETRAYAL){
            document.getElementById("dialog_text").innerHTML='<span id="user_choice">Vous avez coopéré</span><span id="cpu_choice">Le CPU vous a trahi</span><span id="coin">Pièce gagné: 1</span>';
        }
    }

    function end_game () {
        document.getElementById("betrayal_button").style.display = "none";
        document.getElementById("coop_button").style.display = "none";
        //document.getElementById("dialog_box").style.display = "none";
        if (user_score == cpu_score) {
            document.getElementById("final_result").innerText = "Egalité!\nVotre score est de : " + user_score + " Le score du CPU est de : " + cpu_score;
        } else if (user_score > cpu_score) {
            document.getElementById("final_result").innerText = "Vous avez gagné la partie!\nVotre score est de : " + user_score + " Le score du CPU est de : " + cpu_score;
        } else {
            document.getElementById("final_result").innerText = "Vous avez perdu la partie, le CPU a été plus fort que vous!\nVotre score est de : " + user_score + " Le score du CPU est de : " + cpu_score;
        }
        document.getElementById("replay").style.display = "block";
        if (hard_mode == true) {
            document.getElementById('cpu_strategy').innerText= "La stratégie du CPU était: " + cpu_strategy.name.replace("_", "-");
        }
        send_score();
    }

    function display_round () {
        let set_round = document.getElementById("round");
        set_round.innerText="Round " + current_round + "/15";
    }


    function send_score() { // Envoyer score de fin de partie en historical (Ajax)
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
               
            }
        };
        let hard_mode_binary;
        if (hard_mode == false) { // convertir true/false en 0/1
            hard_mode_binary = 0;
        } else {
            hard_mode_binary = 1;
        }
        xhttp.open("GET", "ajax.php?score="+ user_score + "&level=" + hard_mode_binary + "&arr_coop=" + user_data.join(","), true);
        xhttp.send();
    }

    function get_cpu_choice () {
        return cpu_strategy(current_round, user_data);
    }

    function get_cpu_strategy () { // random choice cpu parmi les strategies 
        let strat_name= strategy[Math.floor(Math.random() * (strategy.length -1))];
        return strat_name;
    }
  
    

    function show_graph () {
        user_score_table.push(user_score);
        cpu_score_table.push(cpu_score);
        let ctx = document.getElementById('graph_game').getContext('2d');
        let graph_game = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                datasets: [{
                    label: 'Votre Score',
                    data: user_score_table,
                    backgroundColor: [
                        'rgba(44, 130, 201, 0.2)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                    ],
                    borderColor: [
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                    ],
                    borderWidth: 1
                },
                {
                    label: 'CPU Score',
                    data: cpu_score_table,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }],
                
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        // Affiche les score dans la dialog box
        document.getElementById("user_score").innerText="Votre score: "+user_score;
        document.getElementById("cpu_score").innerText="Score du CPU: "+cpu_score;
    }

    function show_detail(i) {
        document.getElementsByClassName("button_detail").innerHTML=document.getElementById("detail_value"+i);
        document.getElementById("detail_value"+i).style.display="block"; 
    }
    function hide_detail(i){
        document.getElementById("detail_value"+i).style.display="none";   
    } 


    // =============================================================================================

    // STRATEGIES

    function show_gameplay_round () { // recuperer les boutons quand ça a ete clique
        let strategy_box_children = document.getElementsByClassName("Strategy_box")[0].children;
        for (let i = 0; i < strategy_box_children.length; i++) {
            strategy_box_children[i].onclick=function () { // recuperer la stratégie clique et cacher les autres strategies + afficher ce qu'on doit jouer au premier tour
                console.log(this.id);
                user_strategy = this.id;
                for (let y = 0; y < strategy_box_children.length; y++) {
                    if (strategy_box_children[y].id != user_strategy) {
                        strategy_box_children[y].style.display="none";
                    }
                }
                display_user_strategy();
            }
        }
        
    }

    function display_user_strategy () { // mettre en surbrillance le bouton adequat
        if (user_strategy == 0) {
            return;
        }
        for (let i = 0; i < strategy.length; i++) {
            if (user_strategy == strategy[i].name) {
                if(strategy[i](current_round, cpu_data) == BETRAYAL) {
                    document.getElementById('betrayal_button').style.cssText= "background-color: green; color: white;";
                    document.getElementById('coop_button').style.cssText= "background-color: red; color: white;";
                } else {
                    document.getElementById('betrayal_button').style.cssText= "background-color: red; color: white;";
                    document.getElementById('coop_button').style.cssText= "background-color: green; color: white;";
                }
            }
        }

    }

    function show_strategy (button, input) {
        let new_div = document.createElement('div');
        new_div.setAttribute('class', 'info_strategy');
        let $button = $(button);
        let offset = $button.offset();
        new_div.style.left = (offset.left + $button.width()) + "px";
        new_div.style.top = offset.top + "px";
        let new_content = document.createTextNode(input);
        new_div.appendChild(new_content);
        button.after(new_div);
    }

    function hide_strategy() {
        let my_elements = document.getElementsByClassName('info_strategy');
        for (let i=0; i<my_elements.length; i++){
            my_elements[i].remove();
        }
    }

    function lunatique(){
        return Math.round(Math.random());
    }

    function periodique_mechant(current_round, previous_gameplay) {
        let choice = [BETRAYAL, BETRAYAL, COOPERATION];
        return choice[(current_round) % 3] // utilise modulo pour le systeme periodique (ex : 0%3 = 0, 1%3 = 1, 2%3 = 2, 3%3 = 0 ect...) 

    }

    function periodique_gentil (current_round, previous_gameplay) {
        let choice = [COOPERATION, COOPERATION, BETRAYAL];
        return choice[(current_round) % 3]
    }

    function binaire (current_round, previous_gameplay) {
        let choice = [COOPERATION, BETRAYAL];
        return choice[(current_round) % 2]
    }

    function cccct (current_round, previous_gameplay) {
        let choice = [COOPERATION, COOPERATION, COOPERATION, COOPERATION, BETRAYAL];
        return choice[(current_round) % 5]
    }
    function donnant_donnant (current_round, previous_gameplay) {
        if (current_round == 0) {
            return COOPERATION;
        } 
        return previous_gameplay[previous_gameplay.length-1];

    }

    function majorite (current_round, previous_gameplay) {
        if (current_round == 0) {
            return COOPERATION;
        }

        let accumulate_coop = 0;

        for (let i = 0; i < previous_gameplay.length; i++) {
            if (previous_gameplay[i] == COOPERATION) {
                accumulate_coop++;
            }
        }

        let accumulate_betrayal = previous_gameplay.length - accumulate_coop;

        if (accumulate_betrayal == accumulate_coop) {
            return COOPERATION;
        } else if (accumulate_betrayal > accumulate_coop) {
            return BETRAYAL;
        } else {
            return COOPERATION;
        }
    }

    function sondeur (current_round, previous_gameplay) {
        if (current_round == 0) {
            return BETRAYAL;
        } else if (current_round == 1 || current_round == 2) {
            return COOPERATION;
        }

        if (previous_gameplay[0] == COOPERATION && previous_gameplay[1] == COOPERATION) {
            return BETRAYAL;
        } else {
            return donnant_donnant(current_round, previous_gameplay);
        }

    }

    // ===========================================================================================
    // UPDATE SCORE

    function update_score (player1_action, player2_action, player1_score) {
        if (player1_action == COOPERATION && player2_action == COOPERATION) {
            player1_score+=3;
        } else if (player1_action == BETRAYAL && player2_action == COOPERATION) {
            player1_score+=5;         
        } else if (player1_action == COOPERATION && player2_action == BETRAYAL){
            player1_score+=1;
        } else if (player1_action == BETRAYAL && player2_action == BETRAYAL) {
            player1_score+=1;
        }
        return player1_score;
    }

    //===============================================================================================

    // GRAPH GAME



    let ctx = document.getElementById('graph_game').getContext('2d');
    let graph_game = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            datasets: [{
                label: 'Votre Score',
                data: user_score_table,
                backgroundColor: [
                        'rgba(44, 130, 201, 0.2)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                    ],
                    borderColor: [
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                        'rgba(44, 130, 201, 1)',
                    ],
                borderWidth: 1
            },
            {
                label: 'CPU Score',
                data: cpu_score_table,
                backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                borderWidth: 1
            }],
            
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    function display_score () {
        // Stocke les scores dans des variables
        user_score=update_score(user_data[user_data.length-1], cpu_data[cpu_data.length-1], user_score);
        cpu_score=update_score(cpu_data[cpu_data.length-1], user_data[user_data.length-1], cpu_score);
        // Affiche les score dans la dialog box
        document.getElementById("user_score").innerText="Votre score: "+user_score;
        document.getElementById("cpu_score").innerText="Score du CPU: "+cpu_score;
    }


</script>
</body>
</html>