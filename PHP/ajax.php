<?php 

session_start();
include "request.php";

function send_score ($score, $level, $arr_coop) {
    if ($score > 90 || $score < 0 || $level < 0 ||$level > 1) {
        return;
    }
    $arr_coop = explode(',', $arr_coop);
    foreach ($arr_coop as $value) {
        if (!in_array($value, ["0", "1"])) {
            return;
        }
    }
    $id_game = add_finished_game($_SESSION['id_user'], $score, $level);
    add_round_gameplay($_SESSION['id_user'], $id_game, $arr_coop);
}

// Vérifier qu'on récupère bien toutes les infos pour ajouter un score
if (
    isset($_SESSION["id_user"]) 
    && isset($_GET['score']) 
    && isset($_GET['level']) 
    && isset($_GET['arr_coop'])
) { 
    send_score(
        intval($_GET['score']),
        intval($_GET['level']),
        (string) $_GET['arr_coop']
    );
}