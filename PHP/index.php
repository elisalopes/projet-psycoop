<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../CSS/index.css" media="all"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <div id='main'>
      <header>
        <h1>PsyCoop</h1>
      </header>
      <?php
      session_start();
      // Se charge de faire la redirection
      if (!empty($_SESSION['connected']) || !empty($_COOKIE['connected'])) {
        header("location: acceuil.php");
      }
      include "request.php";
      include "login.php";
      include "register.php";
      
      ?>
    </div>
    <script>
      // Get the modal
      var modal1 = document.getElementById('inscription');
      var modal2 = document.getElementById('login');
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == modal1 || event.target == modal2) {
              modal1.style.display = "none";
              modal2.style.display = "none";
          }
      }
    </script>
  </body>
</html>
