<button class="bouton_header" onclick="document.getElementById('login').style.display='block'">Login</button>

<!-- Formulaire de connexion -->
<div id="login" class="modal">
  <form class="modal-content animate" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
    <div class="imgcontainer">
      <span onclick="document.getElementById('login').style.display='none'" class="close" title="Close Modal">&times;</span>
      <!-- <img src="logo.png" alt="Avatar" class="avatar"> -->
    </div>

    <div class="container">
      <label for="username"><b>Username</b></label>
      <input type="text" placeholder="Username" name="username">

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Password" name="password">
        
      <button type="submit" name="submit">Login</button>
      <label>
        <input type="checkbox" checked="checked" name="remember"> Se souvenir de moi
      </label>
    </div>
  </form>
</div>


<?php
$handler = mysql_connection();

$errors1 = array();

function protect2($handler, $param) {
    return mysqli_real_escape_string($handler, stripslashes($param));
}

// Quand le boutton du formulaire à été cliqué
if (isset($_POST['submit'])) {
    $username = protect2($handler, $_POST['username']);
    $password = protect2($handler, $_POST['password']);
    $password_hash = password_hash($password, PASSWORD_BCRYPT);

    // Gestion d'erreurs
    if (empty($username)) { 
        array_push($errors1, "Veuillez entrer votre username"); 
    }
    if (empty($password)) { 
        array_push($errors1, "Veuillez entrer votre password"); 
    }

    // Si le formulaire à été correctement rempli
    if (!empty($username) && !empty($password)) {      
        $requete_select_password = "SELECT password FROM user WHERE username='$username'";
        $requete_password=mysqli_query($handler, $requete_select_password);
        $value_password=$requete_password->fetch_assoc();
        $password_bdd= $value_password['password'];
        $POSTpassword=protect2($handler, $_POST['password']);

        // Si la le mot de passe correspond à l'username entré
        if (password_verify($POSTpassword, $password_bdd)) {
            // Si la case Remember Me à été cochée
            if (isset($_POST['remember'])) {
              // Stock l'id de l'utilisateur dans le cookie
              $select_id_user="SELECT id_user FROM user WHERE username='$username'";
              $requete_select_id_user=mysqli_query($handler, $select_id_user);
              $value_id_user=$requete_select_id_user->fetch_assoc();
              $id_user = $value_id_user['id_user'];
              setcookie('id_user', $id_user, strtotime("1 year"));
              // Setup un cookie d'un an
              $_SESSION["id_user"]= $id_user;
              setcookie("connected", TRUE, strtotime("1 year"));
              $_COOKIE['connected']= TRUE;
            }
          // Stock l'id de l'utilisateur dans un cookie
          $select_id_user="SELECT id_user FROM user WHERE username='$username'";
          $requete_select_id_user=mysqli_query($handler, $select_id_user);
          $value_id_user=$requete_select_id_user->fetch_assoc();
          $id_user = $value_id_user['id_user'];
          
          $_SESSION['username'] = $username;
          $_COOKIE['username'] = $username;
          $_SESSION['connected'] = TRUE;
          $_COOKIE['connected']= TRUE;
          header("location: acceuil.php");
        } else {
            // Ajoute une erreur dans le tableau d'erreurs
            array_push($errors1, "La combinaison username/password n'est pas accepté");
        } 
    }
    
    // Affichage du tableau d'erreurs
    if (count($errors1) > 0) { ?>
        <div class="error">
        <?php foreach ($errors1 as $key=>$value) { ?>
            <p><?=$value; ?></p>
        <?php } ?>
        </div>
        <?php  }
} 
