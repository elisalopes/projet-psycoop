<?php 

session_start();
include 'request.php';

if (empty($_COOKIE["connected"]) && empty($_SESSION["connected"])) {
    header("location: index.php");
}
if (isset($_POST['disconnect'])) {  
    setcookie('connected', FALSE);  
    $_COOKIE["connected"]=FALSE;
    $_SESSION["connected"]=FALSE;
    header("location: index.php");
}
if (isset($_POST['retour'])) {  
    header("location: acceuil.php");
}

?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../CSS/index.css" media="all"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id='main'>
            <header>
                <div>
                    <form action="<?=$_SERVER["PHP_SELF"]; ?>"method="POST">
                        <button class='bouton_header' type="submit" name="disconnect">Disconnect</button>
                        <button class='bouton_header' type="submit" name="retour">Retour</button>
                    </form>
                    <h1>Classement</h1>
                </div>
            
         
            </header>
            <div class="ranking_table">

                    <button class="game_mode" name="easy_mode" id="button_easy_mode">Easy Mode</button>
                    <button class="game_mode" name="hard_mode" id="button_hard_mode">Hard Mode</button>
                    <button class="game_mode" name="total" id="button_total">Total</button>
                
                <div id="ranking_easy_mode">
                    <h1>Easy Mode</h1>
                    <table>
                        <tr>
                            <th>Podium</th>
                            <th>Username</th>
                            <th>Score</th>
                            <th>Détails</th>
                        </tr>
                        <?php
                        $ranking = get_ranking_level(0);
                        for ($i = 0; $i < count($ranking); $i++) {
                            $percent_coop = get_percentage_coop($ranking[$i]['id_game']);?>
                            <tr>
                                <td><?=($i + 1);?></td>
                                <td><?=$ranking[$i]['username'];?></td>
                                <td><?=$ranking[$i]['score'];?></td>
                                <td><button class='Strategy_box' onmouseover="show_detail(0, <?=$i;?>)" onmouseout="hide_detail(0, <?=$i;?>)">Détails</button></td>
                                <div id ="detail_value0<?=$i;?>" style="display: none" class="info_strategy">
                                    <?="Coop: ".round($percent_coop) . "% <br>Trahison: " . round(100 - $percent_coop) . "%<br>Nombre de partie: ".$ranking[$i]['count_game'];?>
                                </div>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <div id="ranking_hard_mode" style="display: none;">
                    <h1>Hard Mode</h1>
                    <table>
                        <tr>
                            <th>Podium</th>
                            <th>Username</th>
                            <th>Score</th>
                            <th>Détails</th>
                        </tr>
                        <?php
                        $ranking = get_ranking_level(1);
                        for ($i = 0; $i < count($ranking); $i++) {
                            $percent_coop = get_percentage_coop($ranking[$i]['id_game']);?>
                            <tr>
                                <td><?=($i + 1);?></td>
                                <td><?=$ranking[$i]['username'];?></td>
                                <td><?=$ranking[$i]['score'];?></td>
                                <td><button class='Strategy_box' onmouseover="show_detail(1, <?=$i;?>)" onmouseout="hide_detail(1, <?=$i;?>)">Détails</button></div></td>
                                <div id ="detail_value1<?=$i;?>" style="display: none" class="info_strategy">
                                    <?="Coop: ".round($percent_coop) . "% <br>Trahison: " . round(100 - $percent_coop) . "%<br>Nombre de partie: ".$ranking[$i]['count_game'];?>
                                </div>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
                <div id="ranking_total" style="display: none;">
                    <h1>Total</h1>
                    <table>
                        <tr>
                            <th>Podium</th>
                            <th>Username</th>
                            <th>Score</th>
                            <th>Détails</th>
                        </tr>
                        <?php
                        $ranking = get_ranking();
                        for ($i = 0; $i < count($ranking); $i++) {
                            $percent_coop = get_percentage_coop($ranking[$i]['id_game']);
                            if ($ranking[$i]['level'] == 0) {
                                $level_name = "Easy";
                            } else {
                                $level_name = "Hard";
                            }
                            ?>
                            <tr>
                                <td><?=($i + 1);?></td>
                                <td><?=$ranking[$i]['username'];?></td>
                                <td><?=$ranking[$i]['score'];?></td>
                                <td><button class='Strategy_box' onmouseover="show_detail(3, <?=$i;?>)" onmouseout="hide_detail(3, <?=$i;?>)">Détails</button></div></td>
                                <div id="detail_value3<?=$i;?>" style="display: none" class="info_strategy">
                                    <?="Coop: ".round($percent_coop) . "% <br>Trahison: " . round(100 - $percent_coop) . "%<br>Nombre de partie: ".$ranking[$i]['count_game']."<br>Difficulté: ".$level_name;?>
                                </div>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $("#button_easy_mode").click(function(){
                    document.getElementById("ranking_total").style.display="none";
                    document.getElementById("ranking_hard_mode").style.display="none";
                    document.getElementById("ranking_easy_mode").style.display="block";
                });
                $("#button_hard_mode").click(function(){
                    document.getElementById("ranking_total").style.display="none";
                    document.getElementById("ranking_easy_mode").style.display="none";
                    document.getElementById("ranking_hard_mode").style.display="block";
                });
                $("#button_total").click(function(){
                    document.getElementById("ranking_easy_mode").style.display="none";
                    document.getElementById("ranking_hard_mode").style.display="none";
                    document.getElementById("ranking_total").style.display="block";
                });
            });

            function show_detail(level, i) {
                if (level==0) {
                    document.getElementsByClassName("Strategy_box").innerHTML=document.getElementById("detail_value0"+i);
                    document.getElementById("detail_value0"+i).style.display="block"; 
                } else if (level==1) {
                    document.getElementsByClassName("Strategy_box").innerHTML=document.getElementById("detail_value1"+i);
                    document.getElementById("detail_value1"+i).style.display="block"; 
                } else if (level==3) {
                    document.getElementsByClassName("Strategy_box").innerHTML=document.getElementById("detail_value3"+i);
                    document.getElementById("detail_value3"+i).style.display="block"; 
                }
            }
            function hide_detail(level, i){
                if (level==0){
                    document.getElementById("detail_value0"+i).style.display="none";   
                } else if (level==1) {
                    document.getElementById("detail_value1"+i).style.display="none";   
                } else if (level==3) {
                    document.getElementById("detail_value3"+i).style.display="none";   

                }
            } 

        </script>
    </body>
</html>
