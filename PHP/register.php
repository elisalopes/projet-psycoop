<button class="bouton_header" onclick="document.getElementById('inscription').style.display='block'">Register</button>

<?php


$handler = mysql_connection();
$username ="";
$errors2 = array();

// Fonction de protection des formulaires
function protect1($handler, $param) {
    return mysqli_real_escape_string($handler, stripslashes($param));
}



// Quand le boutton du formulaire est cliqué
if (isset($_POST["inscription"])) {

    $username = protect1($handler, $_POST['username']);
    $password_1 = protect1($handler, $_POST['password_1']);
    $password_2 = protect1($handler, $_POST['password_2']);

    // Gestion d'erreurs
    if (empty($username)) { 
        array_push($errors2, "Un username est obligatoire"); }
    if (empty($password_1)) { 
        array_push($errors2, "Un password est obligatoire"); }
    if ($password_1 !== $password_2) {
        array_push($errors2, "Les deux password ne correspondent pas");
    }

    // Verif username déjà pris
    $request_verif = "SELECT username FROM user WHERE username='$username' LIMIT 1";
    $result_request_verif = mysqli_query($handler, $request_verif);
    $user = $result_request_verif->fetch_all(MYSQLI_ASSOC);
    if ($user) {
        for ($i=0; $i<count($user); $i++) {
            if ($user[$i]['username'] === $username) {
                array_push($errors2, "L'username est déjà pris");
            }

        }
        
    }

    // Verication qu'il n'y ait pas d'erreur
    if (count($errors2) == 0) {
        // Insert dans la BDD
        $password_hash = password_hash($password_1, PASSWORD_BCRYPT);
        $request_insert = "INSERT INTO user (username, password) VALUES 
        ('$username', '$password_hash')";
        mysqli_query($handler, $request_insert);
        // Stock l'id de l'utilisateur dans un cookie
        $select_id_user="SELECT id_user FROM user WHERE username='$username'";
        $requete_select_id_user=mysqli_query($handler, $select_id_user);
        $value_id_user=$requete_select_id_user->fetch_assoc();
        $id_user = $value_id_user['id_user'];
        $_COOKIE['id_user']= $id_user;
        
        $_SESSION['username'] = $username;
        $_COOKIE['username'] = $username;
        $_SESSION['connected'] = TRUE;
        $_COOKIE['connected']= TRUE;
        echo "<script>location.href='acceuil.php'</script>";
    }
}?>

<!-- Affichage du formulaire d'inscription -->
<div id="inscription" class="modal">  
        <form class="modal-content animate" action="<?= $_SERVER["PHP_SELF"]; ?>" method="POST">
            <div class="imgcontainer">
                <span onclick="document.getElementById('inscription').style.display='none'" class="close" title="Close Modal">&times;</span>
                <!-- <img src="logo" alt="Avatar" class="avatar"> -->
            </div>
            <div class="container">
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Tapez votre username" name="username">
                <label for="password_1"><b>Password</b></label>
                <input type="password" placeholder="Tapez votre password" name="password_1">
                <label for="password_2"><b>Password</b></label>
                <input type="password" placeholder="Retapez votre password" name="password_2">
                <?php  if (count($errors2) > 0) { ?>
                <div class="error">
                <?php foreach ($errors2 as $key=>$value) { ?>
                    <p><?=$value; ?></p>
                <?php } ?>
                </div>
                <?php  } ?>
                <button name="inscription">S'inscrire</button>
            </div>
        </form>
        
    </div>