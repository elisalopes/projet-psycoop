<?php 

function mysql_connection () { // connexion a la base de données 
    $handler = mysqli_connect("localhost:3308", "root", "", "PsyCoop");

    if (!$handler) {
        echo "erreur de connexion!";
        die;
    }

    return $handler;
}

function add_finished_game ($id_user, $score, $level) { // Insérer score, level en bdd
    $handler = mysql_connection();
    mysqli_query($handler, "INSERT INTO historical ( `id_user`, `score`, `level` ) VALUES ($id_user, $score, $level)");
    $last_id = mysqli_insert_id($handler); // pour id_game
    mysqli_close($handler);
    return $last_id;
}


function add_round_gameplay($id_user, $id_game, $arr_coop) { // ajouter 15 tours de gameplay en bdd
    $handler = mysql_connection();
    $sql = "INSERT INTO gameplay ( `id_user`, `id_game`,`round`, `coop` ) VALUES ";
    for ($i = 0; $i < count($arr_coop); $i++) {
        $sql .= "($id_user, $id_game, $i, $arr_coop[$i])";
        if ($i < count($arr_coop) -1 ) {
            $sql .= ',';
        }
    }
    echo $sql;
    $query = mysqli_query($handler, $sql) or die(mysqli_error($handler));
    mysqli_close($handler);
}

function get_historical ($id_user) { // Récuperer historique pour page 2
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT level, score, id_game FROM historical WHERE id_user=$id_user ORDER BY historical.id_game DESC LIMIT 5");
    $result = $query->fetch_all(MYSQLI_ASSOC);
    mysqli_close($handler);
    return $result; 
}

function get_percentage_coop($id_game) {
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT COUNT(coop) FROM gameplay WHERE id_game=$id_game AND coop=1");
    $result = $query->fetch_all();
    mysqli_close($handler);
    $value = $result[0][0];
    $percent = (100*$value)/15;
    return $percent;
}

// TODO : Terminé requete historique avec jointure pour coop (calcul pourcentage coop et trahison)

function get_user_number_game ($id_user, $level) { // Récupérer nombre de parties par utilisateurs
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT COUNT(id_game) FROM historical WHERE id_user=$id_user AND level=$level") or die(mysqli_error($handler));
    $result = $query->fetch_array();
    mysqli_close($handler);
    return $result[0];
}


function get_ranking_level ($level) { // Récupérer le classement en fonction du level
    $handler = mysql_connection();
    $query = mysqli_query($handler, 
        "SELECT
        historical.id_user,
        historical.level,
        ANY_VALUE(historical.id_game) as id_game,
        MAX(historical.score) AS score,
        user.username,
                (
                    SELECT COUNT(hist_user.id_game) 
                    FROM historical AS hist_user
                    WHERE hist_user.id_user=historical.id_user AND level=$level
                ) AS count_game 
        FROM historical
        INNER JOIN user ON historical.id_user = user.id_user
        WHERE level=$level
        GROUP BY id_user
        ORDER BY score DESC
        ") or die(mysqli_error($handler));
    $result = $query->fetch_all(MYSQLI_ASSOC);
    mysqli_close($handler);
    return $result;
    
}

function get_ranking () { // Récupérer le classement total
    $handler = mysql_connection();
    $query = mysqli_query($handler, 
        "SELECT
        historical.id_user,
        ANY_VALUE(historical.level) as level,
        ANY_VALUE(historical.id_game) as id_game,
        MAX(historical.score) AS score,
        user.username,
                (
                    SELECT COUNT(hist_user.id_game) 
                    FROM historical AS hist_user
                    WHERE hist_user.id_user=historical.id_user
                ) AS count_game 
        FROM historical
        INNER JOIN user ON historical.id_user = user.id_user
        GROUP BY id_user
        ORDER BY score DESC
        ") or die(mysqli_error($handler));
    $result = $query->fetch_all(MYSQLI_ASSOC);
    mysqli_close($handler);
    return $result;
    
}






 


