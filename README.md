# PsyCoop

## Présentation du projet :

PsyCoop est un projet qui nous a été proposé par notre formateur. Le sujet initial étant le dilemme du
prisonnier, nous avions la liberté de choisir sous quelle forme réaliser l’application web.
Le dilemme du prisonnier est une théorie des jeux visant à opposer deux joueurs ayant soit la possibilité de
coopérer soit de se trahir.
C’est ainsi que nous avons choisi de le réaliser de manière ludique. L’application est de ce fait composé
d’un accès utilisateur qui pourra participer sous forme de parties à la concrétisation du dilemme face à un
bot. Afin de challenger l’utilisateur, un classement a été mis en place tout comme une difficulté de niveau
(easy et hard mode).

## Installation

- Penser à modifier le fichier request.php dans le dossier PHP
```php
function mysql_connection () { // connexion a la base de données 
    $handler = mysqli_connect("localhost:3308", "root", "", "PsyCoop");

    if (!$handler) {
        echo "erreur de connexion!";
        die;
    }

    return $handler;
}
```

## Todo List : 

- <span style="color:red">Corriger les bugs</span>
    * Vérifer le graphique, point manquant
    * Ajouter ratio total nombre de partie/gain
    * Ajouter stratégie du CPU dans l'historique
    * Explication du gameplay (feedback utilisateur)
<br>

## Credits
- Elisa LOPES 
- Boris AHUMADA